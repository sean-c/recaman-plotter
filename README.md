# Recaman Plotter

This project aims to illustrate the [Recaman Sequence](https://en.wikipedia.org/wiki/Recam%C3%A1n%27s_sequence) by plotting it in visually appealing ways.

## The Scripts

This project consists of two scripts
+ `plot_random.py` creates a window and produces random plots in that window.
+ `gui.py` has a basic GUI to control output (work in progress).

`recaman_sequence.py` is a library with functions used for calculating an n-length Recaman Sequence, this is referenced by the scripts.

## Screenshots

![step 1](screenshots/1.png)
![step 2](screenshots/2.png)
![step 3](screenshots/3.png)
