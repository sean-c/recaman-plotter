#!/usr/bin/env python

# Copyright 2018, Sean Clouston
#
# This file is part of Recaman Plotter.
#
#    Recaman Sequence Visualisation is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.s
#
#    Recaman Sequence Visualisation is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Recaman Sequence Visualisation.  If not, see
#    <https://www.gnu.org/licenses/>.

import sys
import os
import numpy as np
import tkinter as tk
from random import randint

# import from current folder
sys.path.append(os.getcwd())
import recaman_sequence as rs


class Arc:
    ''' The class defining an arc drawn from one number to another,
        tilted to a given angle'''

    def __init__(self, canvas, origin, n1, n2, ang, unit_length, asthetics):
        # define canvas parameters
        self.canvas = canvas

        # define basis for coordinate system
        self.origin = origin
        self.u_len = unit_length
        self.angle_deg = ang
        self.angle = np.radians(ang)

        # define integers
        self.n1, self.n2 = n1, n2

        # define pre-translation cartesian points based on integers
        self.x1 = self.point(self.n1)
        self.x2 = self.point(self.n2)

        # define centre point
        self.centre_pt = self.centre(self.x1, self.x2)

        # define arc radius
        self.radius = abs(n1 - n2) * self.u_len/2

        # define arc properties
        self.arc = self.arc_bound(self.centre_pt)
        self.start_ext = 359 if abs(n2 - n1) % 2 == 0 else 179

        # look up asthetic parameters from Asthetics class
        self.asthetics = asthetics
        self.arc_thickness = asthetics.arc_thickness
        self.arc_colour = asthetics.arc_colour


    def point(self, integer):
        ''' creates a point from an integer '''
        return (self.origin[0] + integer * self.u_len * np.cos(self.angle),
                self.origin[1] + integer * self.u_len * np.sin(self.angle))

    def centre(self, x1, x2):
        ''' defines centre point for the arc '''
        self.centre_pt = (x1[0] + x2[0])/2, (x1[1] + x2[1])/2
        return self.centre_pt

    def arc_bound(self, centre_pt):
        ''' defines boundary box for the arc '''
        if self.n2 > self.n1:
            p1 = (centre_pt[0] - self.radius, centre_pt[1] - self.radius)
            p2 = (centre_pt[0] + self.radius, centre_pt[1] + self.radius)
            arc = p1, p2
            return arc
        else:
            p1 = (centre_pt[0] - self.radius, centre_pt[1] + self.radius)
            p2 = (centre_pt[0] + self.radius, centre_pt[1] - self.radius)
            arc = p1, p2
            return arc

    def draw(self):
        ''' draws self.arc on the canvas '''
        self.canvas.create_arc(self.arc, extent=181,
                start=self.start_ext - self.angle_deg, style=tk.ARC,
                width=self.arc_thickness, outline=self.arc_colour)
        self.canvas.update()


class Asthetics:
    ''' Asthetics class deals with asthetic parameters  '''

    def __init__(self, arc_thickness, arc_colour, background_colour):
        if not arc_thickness:
            self.arc_thickness = randint(1, 10)
        else:
            self.arc_thickness = arc_thickness

        if not arc_colour and not background_colour:
            self.arc_colour = self.random_colour()
            self.background_colour = self.colour_complement(self.arc_colour)
        elif (not arc_colour and background_colour):
            self.arc_colour = self.colour_complement(background_colour)
            self.background_colour = background_colour
        elif (arc_colour and not background_colour):
            self.arc_colour = arc_colour
            self.background_colour = self.colour_complement(arc_colour)
        else:
            self.arc_colour = arc_colour
            self.background_colour = background_colour


    def random_colour(self):
        ''' Returns a random colour '''
        return ( '#' + str(hex(randint(0, 15)))[2:] + str(hex(randint(0, 15)))[2:]
                + str(hex(randint(0, 15)))[2:] + str(hex(randint(0, 15)))[2:]
                + str(hex(randint(0, 15)))[2:] + str(hex(randint(0, 15)))[2:])

    def colour_complement(self, col):
        ''' Returns colour compliment '''
        col = col.replace('#', '')
        rgb = (col[0:2], col[2:4], col[4:6])
        com = ['%02X' % (255 - int(i, 16)) for i in rgb]
        return '#' + ''.join(com)


class Random:
    ''' Gives random values for plot parameters '''

    def __init__(self, root):
        self.win_w = root.winfo_screenwidth()
        self.win_h = root.winfo_screenheight()

        # set random point of origin, constrained to
        d = randint(0,1)
        if d == 1:
            self.origin = (randint(1, 50), randint(1, self.win_h))
            self.angle = self.langle()
        else:
            self.origin = (randint(self.win_w - 50, self.win_w - 10),
                    randint(1, self.win_h))
            self.angle = self.angle_rev()

        # set random unit length, this will set the "zoom" of the plot
        self.unit_length = randint(1, 25)

        # set length of sequence, this should increase as unit length decreases
        self.N = 100 * np.exp(2/self.unit_length)

        # define halfway points
        self.xhalf = self.win_w/2
        self.yhalf = self.win_h/2

        # set animation delay time
        self.delay = randint(50, 150)

    def langle(self):
        ''' Sets the range of possible angles based on the origin (left)'''
        return 45 - (self.origin[1]/self.win_h)*90

    def angle_rev(self):
        ''' Sets the range of possible angles based on the origin (right) '''
        return 135 + (self.origin[1]/self.win_h)*90


def main():
    # call Tk class to create root window
    root = tk.Tk()

    # create a frame (this is needed to create a canvas)
    win_h = root.winfo_screenheight()
    win_w = root.winfo_screenwidth()
    frame = tk.Frame(root, width=win_w, height=win_h)
    frame.pack()

    p = 1
    while True:
        print('Plot ', p)
        p += 1

        # set asthetic parameters
        asthetics = Asthetics(None, None, None)
        print('arc_colour = ', asthetics.arc_colour)
        print('background = ', asthetics.background_colour)
        print('arc_thickness = ', asthetics.arc_thickness)

        # create canvas
        canvas = tk.Canvas(frame, width=win_w, height=win_h,
                           bg=asthetics.background_colour)
        canvas.pack()

        # initiate Random class
        rand = Random(root)

        # define origin
        origin = rand.origin
        angle = rand.angle
        print('origin = ', origin)
        print('angle = ', angle)

        # set start and end point for sequence
        n = 1
        N = int(round(rand.N))
        print('N = ', N)

        # generate sequence
        rec = rs.recaman_sequence(N)

        # set delay (this is like fps)
        delay = rand.delay
        print('delay', delay)

        # set unit length
        unit_length = rand.unit_length
        print('Unit length = ', unit_length)

        # draw arcs after every delay interval
        for n in range(n, len(rec)):
            rec1 = rec[n-1]
            rec2 = rec[n]
            a = Arc(canvas, origin, rec1, rec2, angle, unit_length, asthetics)
            canvas.after(delay, a.draw())

        print('\n')
        canvas.destroy()

    root.mainloop()


if __name__ == '__main__':
    main()
