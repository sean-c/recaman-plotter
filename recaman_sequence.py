#!/usr/bin/env python

# Copyright 2018, Sean Clouston
#
# This file is part of Recaman Plotter.
#
#    Recaman Sequence Visualisation is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    Recaman Sequence Visualisation is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Recaman Sequence Visualisation.  If not, see
#    <https://www.gnu.org/licenses/>.



def recaman_sequence(n):
    ''' This function will return first nth numbers in the Recaman sequence.

    The Recaman sequence is calculated by starting at one and subtracting the
    next number (two). If the result is < 1, or already in the sequence;
    then add the next number instead of subtracting and add this number to the
    sequence.

        1, 3, 6, 2, 7, 13, ...

    '''

    # The sequence at n = 0
    seq = [1]

    # loop from 1 to n
    for nth_number in range(1, n):

        # The last number and current step length need to be known
        last_number = seq[-1]
        step = nth_number + 1

        # The number to test for Recaman sequence membership
        new_number = last_number - step

        # if new_number is strictly positive and not already in seq; add it
        # to seq
        if new_number > 0 and new_number not in seq:
            seq.append(new_number)

        # If new_number can't be added, then add last_number + step
        else:
            seq.append(last_number + step)

    return seq

def recaman_next(rec_set):
    ''' Takes a set of Recaman numbers and finds the next number in the
    Recaman sequence '''

    if 3 not in rec_set:
        rec_set.append(3)
        return 3

    # Load last number in recaman set
    last_number = rec_set[-1]

    # Work out step length for num
    step = abs(last_number - rec_set[-2]) + 1

    # Find the next number to test for Recaman membership
    new_number = last_number - step

    # If new_number is strictly positive and not already in rec_set; add it
    # to rec_set
    if new_number > 0 and new_number not in rec_set:
        rec_set.append(new_number)
        return new_number

    # If new_number is negative or already a recaman number, add
    # last_number + step
    else:
        rec_set.append(last_number + step)
        return last_number + step


