#!/usr/bin/env python

# Copyright 2018, Sean Clouston
#
# This file is part of Recaman Plotter.
#
#    Recaman Sequence Visualisation is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    Recaman Sequence Visualisation is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Recaman Sequence Visualisation.  If not, see
#    <https://www.gnu.org/licenses/>.


import sys
import os

# numpy needed for geometry calculations
import numpy as np
import tkinter as tk
from random import randint

## Import the recaman seuence from local library:
sys.path.append(os.getcwd())
import recaman_sequence as rs
from plot_random import Arc, Random, Asthetics


class GUI:
    ''' Creates the GUI for the progrm '''

    def __init__(self):
        self.root = tk.Tk()

        self.win_y = self.root.winfo_screenheight()
        self.win_x = self.root.winfo_screenwidth()

        self.main_menu()

    def main_menu(self):
        ''' Defines the main menu window '''
        self.btn_plot()
        self.btn_rand()
        self.btn_quit()

    def plot(self):
        ''' Creates an environment where the sequence is displayed
        with controls and scroll bars'''
        lst = self.root.pack_slaves()
        for l in lst:
            l.destroy()

        frame = tk.Frame(gui.root, width=self.win_x, height=self.win_y)
        frame.pack()

        scrollbar_x = tk.Scrollbar(frame, orient=tk.HORIZONTAL)
        scrollbar_x.grid(row=1, column=0, sticky=tk.E + tk.W)

        scrollbar_y = tk.Scrollbar(frame)
        scrollbar_y.grid(row=0, column=1, sticky=tk.N+tk.S)

        asthetics = Asthetics(5, 'white', 'cornflower blue')

        # create canvas
        canvas = tk.Canvas(frame, width=self.win_x, height=self.win_y,
                bg = asthetics.background_colour,
                xscrollcommand=scrollbar_x.set, xscrollincrement=10,
                yscrollcommand=scrollbar_y.set, yscrollincrement=10 )
        canvas.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)

        # set up scroll bars witin canvas
        canvas.config(scrollregion=canvas.bbox(tk.ALL))
        scrollbar_x.config(command=canvas.xview)
        scrollbar_y.config(command=canvas.yview)

        # define origin
        origin = 0, self.win_y

        # set atart and end point for sequence
        n = 1
        N = 1000

        rec = rs.recaman_sequence(N)

        delay = 250

        # draw arcs every second
        for n in range(n, len(rec)):
            rec1 = rec[n-1]
            rec2 = rec[n]
            a = Arc(canvas, origin, rec1, rec2, 305, 10, asthetics)
            canvas.after(delay, a.draw())

    def plot_rand(self):
        ''' Displays the sequence with random parameters '''
        lst = self.root.pack_slaves()
        for l in lst:
            l.destroy()

        frame = tk.Frame(gui.root, width=self.win_x, height=self.win_y)
        frame.pack()

        p = 1
        while True:
            print('Plot ', p)
            p += 1

            # set asthetic parameters
            asthetics = Asthetics(None, None, None)
            print('arc_colour = ', asthetics.arc_colour)
            print('background = ', asthetics.background_colour)
            print('arc_thickness = ', asthetics.arc_thickness)

            # create canvas
            canvas = tk.Canvas(frame, width=self.win_x,
                    height=self.win_y, bg=asthetics.background_colour)
            canvas.pack()

            # initiate Random class
            rand = Random(self.root)

            # define origin
            origin = rand.origin
            angle = rand.angle
            print('origin = ', origin)
            print('angle = ', angle)

            # set start and end point for sequence
            n = 1
            N = int(round(rand.N))
            print('N = ', N)

            # generate sequence
            rec = rs.recaman_sequence(N)

            # set delay (this is like fps)
            delay = rand.delay
            print('delay', delay)

            # set unit length
            unit_length = rand.unit_length
            print('Unit length = ', unit_length)

            # draw arcs after every delay interval
            for n in range(n, len(rec)):
                rec1 = rec[n-1]
                rec2 = rec[n]
                a = Arc(canvas, origin, rec1, rec2, angle, unit_length,
                        asthetics)
                canvas.after(delay, a.draw())

            print('\n')
            canvas.destroy()

    def btn_plot(self):
        ''' Button which calls the plot function '''
        btn_plot = tk.Button(self.root, text='Plot a sequence',
                command=lambda: self.plot())
        btn_plot.pack()

    def btn_rand(self):
        ''' Button which calls the plot_rand function '''
        btn_rand = tk.Button(self.root, text='Plot sequences randomly',
                command=lambda: self.plot_rand())
        btn_rand.pack()

    #TODO: implement back button
    def btn_back(self):
        ''' Button to return to previous page '''
        pass

    def btn_quit(self):
        ''' Button to quit the program '''
        btn_quit = tk.Button(self.root, text='Quit', fg='red',
                command=lambda: self.root.destroy())
        btn_quit.pack()


if __name__ == '__main__':
    gui = GUI()
    gui.root.mainloop()
